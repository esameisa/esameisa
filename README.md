## Hello 👋
<img src="https://komarev.com/ghpvc/?username=esameisa&label=Visitors" alt="Visitors"/>

I help my clients navigate through the fascinating world of user focused experience design to obtain results.

## Back-End Skills
- OOP / Data Structures / Design Patterns
- MySQL / PHP
- Laravel
- Vue.js
## UI/UX & Front-End Skills
- Creating UI / UX Design using Photoshop / Sketch App
- HTML / CSS / JS / JQuery
- SASS / HAMEL / Gulp.js / Webpack
- Bootstrap 3,4,5 / Materializecss / TailwindCSS

## Info
- 🔭 I’m currently working as a **Back-End Developer** at **cowpay.me**, it is a premium payment technology enabler dedicated to helping businesses transform their operation collecting , splitting , and disbursing money digitally!
- 🔭 I’m currently working as a **Back-End Developer** at **techne.me**, it is an international multi-industry focused investment & entrepreneurship event that aims to impact multiple sectors and stakeholders of the startup communities in the Mediterranean region through showcasing different technologies and their application in each industry.
- 🌱 I’m currently learning **tips & tricks in laravel and AWS**
- 👯 I’m looking to collaborate on **startup idea**
- 🤔 I’m looking for **mentor**
- 💬 Ask me about **UI/UX, Front-End, Back-End**
- 📫 How to reach me: <a href="https://www.linkedin.com/in/esameisa/" target="_blank">**linkedin**</a>, <a href="mailto:esameisa12345@gmail.com">**esameisa12345@gmail.com**</a>

  

## How to reach me: 

<a href="https://www.linkedin.com/in/esameisa/">
  <img align="left" alt="Esam Eisa" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
</a>


<a href="https://twitter.com/Eng_esameisa">
  <img align="left" alt="Esam Eisa | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />
</a>


<a href="https://www.facebook.com/Esam.A.Eisa/">
  <img align="left" alt="Esam Eisa" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/facebook.svg" />
</a>


<a href="https://github.com/esameisa/">
  <img align="left" alt="Esam Eisa" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/github.svg" />
</a>
